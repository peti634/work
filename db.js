const mysql = require("mysql");
const pwd = require("./pwd.js");

const PING_INTERVAL = 1 * 60 * 1000; //1 min
pwd.config.connectionLimit = 1;
const pool = mysql.createPool(pwd.config);

//check that db connection is ok
pool.query("SELECT 1;", undefined, err => {
  if (err) {
    console.info("DB connect error:", err);
    process.exit();
  } else {
    console.info("DB connection is OK");

    //set interval to ping
    setInterval(() => {
      pool.getConnection(function(err, connection) {
        connection.ping(function(err) {
          connection.release();
          if (err) {
            console.log("Server error when ping:", err);
          }
        });
      });
    }, PING_INTERVAL);
  }
});

exports.dbQuery = (query, data, callback) => {
  pool.query(query, data, callback);
};
