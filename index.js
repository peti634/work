const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const db = require("./db.js");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  next();
});

app.get("/list", function(req, res) {
  // First read existing users.
  console.info("list request");

  db.dbQuery("SELECT * from nelb.work;", undefined, (err, result) => {
    if (err) {
      console.info(err);
      res.end(JSON.stringify({ error: err }));
    } else {
      res.end(JSON.stringify(result));
    }
  });
});

app.get("/head", function(req, res) {
  // First read existing users.
  console.info("head request");

  db.dbQuery("SHOW FULL COLUMNS FROM work;", undefined, (err, result) => {
    if (err) {
      console.info(err);
      res.end(JSON.stringify({ error: err }));
    } else {
      const convertedArray = result.map(d => ({
        name: d.Field,
        head: d.Comment,
        type: d.Type
      }));
      res.end(JSON.stringify(convertedArray));
    }
  });
});

app.post("/add", function(req, res) {
  console.info("add request");
  console.info(req.body);
  db.dbQuery("INSERT INTO nelb.work SET ?", req.body, (err, result) => {
    if (err) {
      console.info(err);
      res.end(JSON.stringify({ error: err }));
    } else {
      res.end(JSON.stringify({ error: 0 }));
    }
  });
});

app.post("/upd", function(req, res) {
  console.info("upd request ", req.query.id);
  console.info(req.body);

  db.dbQuery(
    "UPDATE nelb.work SET ? WHERE id=?",
    [req.body, req.query.id],
    (err, result) => {
      if (err) {
        console.info(err);
        res.end(JSON.stringify({ error: err }));
      } else {
        res.end(JSON.stringify({ error: 0 }));
      }
    }
  );
});

app.delete("/del", function(req, res) {
  // First read existing users.
  console.info("del request");

  db.dbQuery("DELETE FROM work WHERE id=?", req.query.id, (err, result) => {
    if (err) {
      console.info(err);
      res.end(JSON.stringify({ error: err }));
    } else {
      res.end(JSON.stringify({ error: 0 }));
    }
  });
});

var server = app.listen(4500, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at http://%s:%s", host, port);
});
