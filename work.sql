-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql.omega:3306
-- Generation Time: Nov 18, 2019 at 10:50 PM
-- Server version: 5.6.45-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nelb`
--

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `desc` varchar(50) NOT NULL COMMENT 'Leírás',
  `itemNum` int(11) NOT NULL COMMENT 'Cikk szám',
  `weight` int(11) NOT NULL COMMENT 'Súly',
  `createDate` date NOT NULL COMMENT 'Gyártás ideje',
  `price` int(11) NOT NULL COMMENT 'Ár'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`id`, `desc`, `itemNum`, `weight`, `createDate`, `price`) VALUES
(1, 'alma', 10005, 5, '2019-11-13', 150),
(2, 'barack', 2323, 264, '0000-00-00', 0),
(9, 'meggy', 1234, 22, '2012-10-08', 100),
(10, 'palesz', 12345, 1, '2020-10-08', 1),
(11, 'Szilva (a legjobb)', 1, 666, '0000-00-00', 3000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `work`
--
ALTER TABLE `work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
